package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ShoppingCart implements ShoppingCartOperation {

    List<CartProduct> productList = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        if (productName.equals("") || price <= 0 || quantity <= 0 || productList.size() == ShoppingCart.PRODUCTS_LIMIT) {
            return false;
        } else if (productList.size() == 0) {
            CartProduct cartProduct = new CartProduct(productName, price, quantity);
            productList.add(cartProduct);
            return true;
        }

        for (CartProduct product : productList) {
            if (product.getProductName().equals(productName) && product.getPrice() != price) {
                return false;
            }
            CartProduct cartProduct = new CartProduct(productName, price, quantity);
            productList.add(cartProduct);
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String productName, int quantity) {
        for (CartProduct product : productList) {
            if (product.getProductName().equals(productName) && product.getQuantity() >= quantity) {
                if (product.getQuantity() == quantity) {
                    productList.remove(product);
                    return true;
                }
                product.setQuantity(product.getQuantity() - quantity);
                return true;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        int quantityOfProduct = 0;
        for (CartProduct product : productList) {
            if (product.getProductName().equals(productName)) {
                quantityOfProduct += product.getQuantity();
            }
        }
        return quantityOfProduct;
    }

    public int getSumProductsPrices() {
        int sumOfPrices = 0;
        for (CartProduct product : productList) {
            sumOfPrices += product.getPrice();
        }
        return sumOfPrices;
    }

    public int getProductPrice(String productName) {
        for (CartProduct product : productList) {
            if (product.getProductName().equals(productName)) {
                return product.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        Set<String> listOfProductNames = new HashSet<>();
        for (CartProduct product : productList) {
            listOfProductNames.add(product.getProductName());
        }
        return new ArrayList<String>(listOfProductNames);
    }
}
